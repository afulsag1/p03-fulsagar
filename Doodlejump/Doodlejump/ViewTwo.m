#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ViewTwo:UIViewController
{
    IBOutlet UIButton *start;
    IBOutlet UILabel *label;
    float x1,y1,width,height,lowerbound,upperbound,tempx,tempy;
    int a;
    float x[15],y[15];
    float slidervalue,p;
    UIImageView *dot;
    UIImageView *grid;
    CGPoint startPoint;
    CGPoint endPoint;
    CGRect screenRect;
    CGSize screenSize ;
    CGFloat screenWidth;
    CGFloat screenHeight;
    __weak IBOutlet UILabel *lose;

    __weak IBOutlet UIButton *playagain;
    
}
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak,nonatomic) IBOutlet UISlider *slider;
@end

@implementation ViewTwo
-(void)viewDidLoad
{
    [super viewDidLoad];
    p=0.5;
    //getting screen size
     screenRect = [[UIScreen mainScreen] bounds];
    screenSize = screenRect.size;
     screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    //creating bricks
    
    lowerbound=50.0;
    upperbound=150.0;
    tempx=0.0;
    tempy=0.0;
    /*for(int i=0;i<5;i++)
    {
        x1=arc4random_uniform(screenWidth);
        if(x1<50.0)
            x[i]=x1+50.0;
        if(x1>(screenWidth-20.0))
        {
            x[i]=x1-20.0;
        }
        else
            x[i]=x1;
        y1=arc4random_uniform(screenHeight);
        if(y1<100.0)
            y[i]=y1+100.0;
        if(y1>(screenHeight-100.0))
        {
            y[i]=y1-100.0;
        }
        else
            y[i]=y1;
        //to get the last brick
        if(tempy<y[i])
        {
            tempy=y[i];
            if(x[i]<=(screenWidth-20.0) && x[i]>=(screenWidth-70.0))
                        tempx=x[i]-30.0;
            else
                tempx=x[i];
            //NSLog(@"height:%f",tempy);
        }
        // code to create bricks
        UIView *myBox  = [[UIView alloc] initWithFrame:CGRectMake(x[i], y[i], 100.0, 10.0)];
        myBox.backgroundColor = [UIColor blackColor];
        [self.view addSubview:myBox];
    }*/
    
    UIView *myBox  = [[UIView alloc] initWithFrame:CGRectMake(140.0, (screenHeight-560.0), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor blackColor];
    [self.view addSubview:myBox];
    x[0]=140.0;
    y[0]=screenHeight-560.0;
   
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(300.5, (screenHeight-460), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor greenColor];
    [self.view addSubview:myBox];
    x[1]=300.5;
    y[1]=screenHeight-460;
    
   myBox  = [[UIView alloc] initWithFrame:CGRectMake(100.5, (screenHeight-360), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor blackColor];
    [self.view addSubview:myBox];
    x[2]=100.5;
    y[2]=screenHeight-360;

    myBox  = [[UIView alloc] initWithFrame:CGRectMake(255.5, (screenHeight-260), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor greenColor];
    [self.view addSubview:myBox];
    x[3]=255.5;
    y[3]=screenHeight-260;
    
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(30.5, (screenHeight-160), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor blackColor];
    [self.view addSubview:myBox];
    x[4]=30.5;
    y[4]=screenHeight-160;
    
    //code to create last brick at center of bottom of screen
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(155.5, (screenHeight-60.0), 100.0, 10.0)];
    myBox.backgroundColor = [UIColor greenColor];
   [self.view addSubview:myBox];
    x[5]=155.5;
    y[5]=screenHeight-60.0;
    
    
    //code for inserting the image
    

    dot =[[UIImageView alloc] initWithFrame:CGRectMake(155.5,(screenHeight-110.0),50.0,50.0)];
    dot.image=[UIImage imageNamed:@"newdoodlejump.png"];
    
    [self.view addSubview:dot];
    
    startPoint = dot.center;
    endPoint = dot.center;
    endPoint.y=endPoint.y-100.0;

    a=4;
    [self start];
    
}
- (IBAction)slider:(id)sender {
    //self->label.text = [NSString stringWithFormat:@"%f", self.slider.value];
    
}

-(void)start
{
    //timer code for jump
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(jump:) userInfo:NULL repeats:YES ];
  
  [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(jumpslider:) userInfo:NULL repeats:YES ];

}
-(void)jump:timer
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         if(startPoint.y>(screenHeight-60))
                         {
                            
                             lose.hidden=NO;
                             lose.text=@"!!!YOU LOSE!!!";
                             playagain.hidden=NO;

                         }
                         if(a==-1)
                         {
                           
                             lose.text=@"!!!!YOU WIN!!!!";
                             lose.hidden=NO;
                             playagain.hidden=NO;
                         }
                         if(startPoint.x>=x[a] && startPoint.x<=(x[a]+130))
                         {
                           startPoint.y=endPoint.y;
                             //startPoint.x=slidervalue*327;
                             NSLog(@"starpoint:%@",NSStringFromCGPoint(startPoint));
                             dot.center=startPoint;
                             endPoint.y=endPoint.y-100.0;
                             NSLog(@"endpoint:%@",NSStringFromCGPoint(endPoint));
                             a=a-1;
                             NSLog(@"a:%d",a);
                         }
                         else if(startPoint.x<x[a+1] || startPoint.x>(x[a+1]+130))
                         {
                             startPoint.y=startPoint.y+100;
                             endPoint.y=endPoint.y+100;
                             dot.center=startPoint;
                             a=a+1;
                         }
                         else
                              dot.center=endPoint;
                     }//end of animations
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5
                                          animations:^{
                                                  dot.center=startPoint;
                                              
                                          }//animation ends here
                          ];//uiview ends here
                     }// completion ends here
     ];//uiview ends here
}

-(void)jumpslider:timer
{
    if(self.slider.value!=p)
    {
        self.label.text = [NSString stringWithFormat:@"%f", self.slider.value];
        slidervalue=[self.label.text floatValue];
        NSLog(@"slider value:%f",(slidervalue*327));
            startPoint.x=slidervalue*327;
        endPoint.x=slidervalue*327;
        [UIView animateWithDuration:0.1
                     animations:^{
                         dot.center=endPoint;
                        
                     }//animation ends here
                     completion:^(BOOL finished){
                         
                     }// completion ends here
         ];//uiview ends here
        p=self.slider.value;
    }//if ends here
}// function jumpslider ends here


@end
